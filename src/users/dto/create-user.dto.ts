export class CreateUserDto {
  email: string;

  password: string;

  fullName: string;

  gender: string;

  roles: { id: number; name: string }[];
}
